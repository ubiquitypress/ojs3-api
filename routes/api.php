<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvide within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/journal/{code}', 'JournalController@getDetails')->name('journal-details');
Route::get('/journal/{code}/contact','JournalController@getContactDetails')->name('contact-details');
Route::get('/journal/{code}/article','ArticleController@getArticles')->name('get-articles');
Route::get('/journal/{code}/article/{identifier}/metadata','ArticleController@getArticleMetadata')->name('get-article-metadata');
Route::get('/journal/{code}/article/{identifier}/content','ArticleController@getArticleContent')->name('get-article-content');
