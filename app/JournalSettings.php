<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class JournalSettings extends Model
{
    use \LaravelTreats\Model\Traits\HasCompositePrimaryKey;

    protected $primaryKey = ['journal_id', 'locale', 'setting_name'];

    /**
     * Get the comments for the blog post.
     */
    public function journal()
    {
        return $this->belongsTo('App\Journal', 'journal_id', 'journal_id');
    }
}