<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Journal extends Model
{
    protected $primaryKey = 'journal_id';

    /**
     * Get the settings of a Journal
     */
    public function settings()
    {
        return $this->hasMany('App\JournalSettings', 'journal_id');
    }

    /**
     * Get the sections of a Journal
     */
    public function sections()
    {
        return $this->hasMany('App\Section', 'journal_id');
    }

    /**
     * Get the sections of a Journal
     */
    public function submissions()
    {
        return $this->hasMany('App\Submission', 'context_id');
    }
}