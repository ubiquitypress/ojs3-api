<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubmissionContents extends Model
{
    /**
     * Get the settings of a submission
     */
    public function file()
    {
        return $this->belongsTo('App\SubmissionFiles', 'file_id');
    }
}
