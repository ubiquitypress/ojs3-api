<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $primaryKey = 'review_id';

    /**
     * Get the reviews for the submission
     */
    public function submission()
    {
        return $this->hasOne('App\Submission', 'submission_id', 'review_id');
    }
}
