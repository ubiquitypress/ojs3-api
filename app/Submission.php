<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Submission extends Model
{
    protected $primaryKey = 'submission_id';

    /**
     * Get the settings of a submission
     */
    public function journal()
    {
        return $this->belongsTo('App\Journal', 'submission_id', 'context_id');
    }

    /**
     * Get the author for the submission
     */
    public function author()
    {
        return $this->belongsTo('App\Author', 'submission_id', 'submission_id');
    }

    /**
     * Get the reviews for the submission
     */
    public function review()
    {
        return $this->hasMany('App\Review', 'submission_id');
    }

    /**
     * Get the section of a submission/journal
     */
    public function section()
    {
        return $this->belongsTo('App\Section', 'section_id');
    }

    /**
     * Get the settings of a submission
     */
    public function settings()
    {
        return $this->hasMany('App\SubmissionSettings', 'submission_id');
    }

    /**
     * Get the files of a submission
     */
    public function files()
    {
        return $this->hasMany('App\SubmissionFiles', 'submission_id');
    }
}
