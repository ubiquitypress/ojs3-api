<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubmissionSettings extends Model
{
    use \LaravelTreats\Model\Traits\HasCompositePrimaryKey;

    protected $primaryKey = ['submission_id', 'locale', 'setting_name'];

    /**
     * Get the submission for the file
     */
    public function submission()
    {
        return $this->belongsTo('App\Submission', 'submission_id', 'submission_id');
    }
}
