<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SectionSettings extends Model
{
    use \LaravelTreats\Model\Traits\HasCompositePrimaryKey;

    protected $primaryKey = ['section_id', 'locale', 'setting_name'];

    /**
     * Get the section for the setting
     */
    public function section()
    {
        return $this->belongsTo('App\Section', 'section_id', 'section_id');
    }
}
