<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    protected $primaryKey = 'author_id';

    /**
     * Get the submissions for the author
     */
    public function submissions()
    {
        return $this->hasMany('App\Submission', 'submission_id', 'author_id');
    }

    /**
     * Get the settings of an author
     */
    public function settings()
    {
        return $this->hasMany('App\AuthorSettings', 'author_id');
    }
}
