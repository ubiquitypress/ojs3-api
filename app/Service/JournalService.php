<?php

namespace App\Service;

use App\Journal;
use Illuminate\Database\Eloquent\Collection;

class JournalService
{
    /**
     * @var $mappedAttributes Array of attributes in the form (database-record-attribute => name-to-show) with the name to be exposed in the API
     */
    protected $mainMappedAttributes = array(
        'name' => 'title',
        'subtitle' => 'subtitle',
        'initials' => 'initials',
        'acronym' => 'abbreviation',
        'description' => 'description',
        'about' => 'focus_scope',
        'editorialPolicies' => 'editorial_policies',
        'reviewGuidelines' => 'review_guidelines',
        'archiving' => 'archiving',
        'onlineIssn' => 'eissn',
        'printIssn' => 'print_issn',
        'termsConditions' => 'terms_conditions',
        'privacyStatement' => 'privacy_policy',
        'publisherInstitution' => 'published_by',
    );

    protected $sectionMappedAttributes = array(
        'title' => 'name',
    );

    /**
     * Provides journal details hanlding capabilities
     * 
     * @param Journal $journal Id of the journal to work with
     * 
     * @return array $masterDetails The array with the whole information about the journal
     */ 
    public function getDetails(Journal $journal)
    {
        $masterDetails = array();

        $masterDetails = $this->mapSettings($journal->settings()->select('setting_name', 'setting_value')->get());
        $sections = $this->mapSections($journal->sections()->select('section_id', 'editor_restricted', 'meta_indexed', 'meta_reviewed' )->get());

        $masterDetails['default_language'] = $journal->primary_locale;
        $masterDetails['sections'] = $sections;
        $masterDetails['licenses'] = array('name' => null, 'code' => null, 'link' => null );
        return $masterDetails;
    }

    /**
     * Maps the journal settings retrieved in an appropiate structure for returning as the response
     * 
     * @param Collection $settings The settings retrieved from the Journal
     * 
     * @return array $response The settings mapped as is required
     */
    protected function mapSettings(Collection $settings)
    {

        $response = array_combine($this->mainMappedAttributes,array_fill(0, count($this->mainMappedAttributes), null));

        foreach ($settings as $setting) {
            //var_dump($setting);
            if (in_array($setting->setting_name, array_keys($this->mainMappedAttributes))) {
                $exposedAttribute = $this->mainMappedAttributes[$setting->setting_name];

               $response[$exposedAttribute] = $setting->setting_value;
            }
        }

        return $response;
    }

    /**
     * Maps the journal sections retrieved in an appropiate structure for returning as the response
     * 
     * @param Collection $settings The settings retrieved from the Journal
     * 
     * @return array $response The settings mapped as is required
     */
    protected function mapSections(Collection $sections)
    {
        $response = array();
        $mappedSettings = array();

        foreach ($sections as $section) {

            foreach ($section->settings as $setting) {
                if (in_array($setting->setting_name, array_keys($this->sectionMappedAttributes))) {
                    $exposedAttribute = $this->sectionMappedAttributes[$setting->setting_name];
                    $response[$exposedAttribute] = $setting->setting_value;
                    $response['policy'] = array('open_submissions'=>$section->editor_restricted,'indexed'=>$section->meta_indexed,'peer_reviewed' => $section->meta_reviewed);
                }
            }

            $mappedSettings[] = $response;
        }

        
        
        return $mappedSettings;
    }
}