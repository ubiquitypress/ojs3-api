<?php

namespace App\Service;

use App\Journal;
use App\Submission;
use App\Author;
use Illuminate\Database\Eloquent\Collection;

class ArticleService
{
    /**
     * @var $mappedAttributes Array of attributes in the form (database-record-attribute => name-to-show) with the name to be exposed in the API
     */
    protected $mappedAttributes = array(
        'title' => 'title',
        'abstract' => 'abstract',
    );

    /**
     * @var $mappedAttributes Array of attributes in the form (database-record-attribute => name-to-show) with the name to be exposed in the API
     */
    protected $mappedAuthorSettingsAttributes = array(
        'givenName' => 'first_name',
        'familyName' => 'last_name',
    );

    /**
     * Performs the fetching of all the articles for a journal
     * 
     * @param Journal $journal Model with the journal information
     * 
     * @return An array with the articles list
     */
    public function fetchAll(Journal $journal)
    {
        $articles = array();

        //TODO Investigate how to properly map a Journal with a Submission
        $submissions = Submission::all();

        foreach ($submissions as $submission) {
            $article = array();
            $article['metadata'] = $this->getArticleMetadata($submission);
            //$article['content'] = $this->getContent($submission);

            $articles[] = $article;
        }

        return $articles;
    }

    /**
     * Performs the fetching of all the articles for a journal
     * 
     * @param Journal $journal Model with the journal information
     * @param Submission $submissionId Id of the article to be fetched
     * 
     * @return An array with the articles list
     */
    public function getMetadata(Submission $submission)
    {
        return $this->getArticleMetadata($submission);
    }

    protected function getMappedSettings($settings)
    {
        $response = array();
        foreach ($settings as $setting) {
            if (in_array($setting->setting_name, array_keys($this->mappedAttributes))) {
                $exposedAttribute = $this->mappedAttributes[$setting->setting_name];

               $response[$exposedAttribute] = $setting->setting_value;
            }
        }

        return $response;
    }

    /**
     * Gets the available metadata for an article
     * 
     * @param Submission $submission The Submission model for fetching the articles
     * 
     * @return An array with the metadata information of the article
     */
    protected function getArticleMetadata(Submission $submission)
    {
        $article = array();

        $article = $this->getMappedSettings($submission->settings);

        $article['id'] = $submission->submission_id;
        $article['doi'] = null;
        $article['submitter'] = array(
            'first_name' => null,
            'last_name' => null,
            'email' => null,
        );
        $section = array();
        $section = $this->getSectionInformation($submission);
        $article['section'] = $section['title'];
        $article['date_submitted'] = $submission->date_submitted;
        $article['date_accepted'] = null;
        $article['date_published'] = null;
        $article['peer_reviewed'] = null;
        $article['authors'] = $this->getAuthor($submission);
        $article['license'] = array(
            'name' => null,
            'code' => null,
            'link' => null
        );
        $article['competing_interests'] = null;

        $article['keywords'] = array();
        return $article;
    }

    /**
     * Gets the information and data about a section belonging to a submission/article
     * 
     * @param Submission $submission The Submission model for fetching the articles
     * 
     * @return An array with the section information of the submission/article
     */
    protected function getSectionInformation(Submission $submission)
    {
        $response = array();

        foreach ($submission->section()->get() as $section) {
            foreach ($section->settings()->get() as $setting) {
                $response[$setting->setting_name] = $setting->setting_value;
            }
        }

        return $response;
    }

    /**
     * Get the available content for an article, from all possible loaded files
     * 
     * @param Submission $submission The Submission/Article model for fetching the articles
     * 
     * @return An array with the content information of the submission/article
     */
    public function getContent(Submission $submission)
    {
        $content = \DB::table('submission_contents AS sc')
            ->join('submission_files AS sf', function ($join) {
                $join->on('sc.file_id', '=', 'sf.file_id');
                $join->on('sc.revision', '=', 'sf.revision');
            })
            ->leftJoin('submission_contents AS sc2', function ($join) {
                $join->on('sc.file_id', '=', 'sc2.file_id');
                $join->on('sc.revision', '<', 'sc2.revision');
            })
            ->whereNull('sc2.file_id')
            ->select('sc.*')
            ->get();

        return $content;
    }

    /**
     * Get the available content for an article, from all possible loaded files
     * 
     * @param Submission $submission The Submission/Article model for fetching the articles
     * 
     * @return An array with the content information of the submission/article
     */
    public function getAuthor(Submission $submission)
    {
        $response = array();

        foreach ($submission->author()->get() as $author) {
            $response['email'] = $author->email;
            foreach ($author->settings()->select('setting_value','setting_name')->get() as $setting) {
                if(in_array($setting->setting_name, array_keys($this->mappedAuthorSettingsAttributes))) {
                    $exposedAttribute = $this->mappedAuthorSettingsAttributes[$setting->setting_name];
                    $response[$exposedAttribute] = $setting->setting_value;    
                }
                
            }
        }
        return $response;
    }

}