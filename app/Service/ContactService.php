<?php

namespace App\Service;

use App\Journal;
use App\JournalSettings;
use Illuminate\Database\Eloquent\Collection;
use App\Exceptions\JournalNotFoundException;

class ContactService
{
    //UK Postcode validation form the UK goverment site
    const UK_POSTCODE_REGEX_VALIDATION = '/^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z])))) [0-9][A-Za-z]{2})$/';

    //Validates an address in the format "<Number> <Street name>"
    const STREET_REGEX_VALIDATOR = '/([\d]+)\s?(.+)/';

    /**
     * @var array $mappedAttributes Attributes in the form (database-record-attribute => name-to-show) with the name to be exposed in the API
     */
    protected $mainMappedAttributes = array(
        'mailingAddress' => 'mailing',
    );

    protected $principalMappedAttributes = array(
        'contactTitle' => 'title',
        'contactEmail' => 'email'
    );

    protected $supportMappedAttributes = array(
        'supportName' => 'title',
        'supportEmail' => 'email',
    );

    /**
     * Provides contact details hanlding capabilities
     * 
     * @param Journal $journal The Journal model to work with
     * 
     * @return array $contactDetails The array with the whole information about the journal
     * @throws JournalNotFoundException If no journal is found
     */ 
    public function getDetails(Journal $journal)
    {
        $contactDetails = array();

        $mailing = $this->getMailingData($journal->settings);
        $contactDetails['principal'] = $this->getPrincipalData($journal->settings);
        $contactDetails['support'] = $this->getSupportData($journal->settings);
        $contactDetails = array_merge($mailing,$contactDetails);
        return $contactDetails;
    }

    protected function getMailingData(Collection $settings)
    {
        $response = array();

        foreach ($settings as $setting) {
            if (in_array($setting->setting_name, array_keys($this->mainMappedAttributes))) {
                $exposedAttribute = $this->mainMappedAttributes[$setting->setting_name];

                if ($this->isMailingAddress($setting->setting_name)) {
                    $response[$exposedAttribute] = $this->processMailingAdress($setting->setting_value);
                } else {
                    $response[$exposedAttribute] = $setting->setting_value;
                }
            }
        }

        return $response;
    }

    protected function getPrincipalData(Collection $settings)
    {
        $response = array();

        foreach ($settings as $setting) {
            if (in_array($setting->setting_name, array_keys($this->principalMappedAttributes))) {
                $exposedAttribute = $this->principalMappedAttributes[$setting->setting_name];

                if ($this->isMailingAddress($setting->setting_name)) {
                    $response[$exposedAttribute] = $this->processMailingAdress($setting->setting_value);
                } else {
                    $response[$exposedAttribute] = $setting->setting_value;
                }
            }
        }

        return $response;
    }

    protected function getSupportData(Collection $settings)
    {
        $response = array();

        foreach ($settings as $setting) {
            if (in_array($setting->setting_name, array_keys($this->supportMappedAttributes))) {
                $exposedAttribute = $this->supportMappedAttributes[$setting->setting_name];

                if ($this->isMailingAddress($setting->setting_name)) {
                    $response[$exposedAttribute] = $this->processMailingAdress($setting->setting_value);
                } else {
                    $response[$exposedAttribute] = $setting->setting_value;
                }
            }
        }

        return $response;
    }

    protected function isMailingAddress($setting)
    {
        return $setting == 'mailingAddress';
    }

    /**
     * Process the mailing address raw field fetched, which could contain postcode and city
     * 
     * @param string $mailingAddress
     * @return array The address components
     */
    protected function processMailingAdress($mailingAddress)
    {
        $matches = array();
        $addressComponents = array();
        $houseInfo = array(
            'street' => '',
            'house_number' => '',
            'city' => '',
            'country_code',
            'country'
        );

        if ($this->isCompleteAddress($mailingAddress)) {
            //Array with <address, postcode> information, if post code is a valid UK one
            $address = explode(',', $mailingAddress);

            $houseInfo = $this->getHouseInfo($address[0]);
            $houseInfo['city'] = trim($address[2]);
            $houseInfo['country_code'] = trim($address[3]);
            $houseInfo['country'] = null;
            $postcodeToValidate = trim($address[1]);
        } else {
            $postcodeToValidate = $mailingAddress;
        }

        preg_match(self::UK_POSTCODE_REGEX_VALIDATION, $postcodeToValidate, $matches);

        $postcode = empty($matches) ? null : $matches[0]; 
        
        $addressComponents['street'] = $houseInfo['street'];
        $addressComponents['house_number'] = $houseInfo['house_number'];
        $addressComponents['postal_code'] = $postcode;
        $addressComponents['city'] = $houseInfo['city'];
        $addressComponents['country_code'] = $houseInfo['country_code'];
        $addressComponents['country'] = $houseInfo['country'];
        $addressComponents['text'] = $mailingAddress;

        return $addressComponents;
    }

    /**
     * Method to check if the address is in the format "<No> Street name, City <Post Code>".
     * 
     * @param string $mailingAddress
     * @return boolean
     */
    protected function isCompleteAddress($mailingAddress)
    {
        return strstr($mailingAddress, ',') == false ? false : true;
    }

    /**
     * Gets (if possible) the components of a full address
     * 
     * @param string $adress The address before the comma to process
     * @return array An array with the elements of the address, if they exist
     */
    protected function getHouseInfo($address)
    {
        $matches = array();
        $houseInfo = array(
            'street' => '',
            'house_number' => '',
            'city' => '',
        );

        preg_match(self::STREET_REGEX_VALIDATOR, $address, $matches);
        
        if (!empty($matches)) {
            $houseInfo['house_number'] = $matches[1];
            $houseInfo['street'] = $matches[2];
        }

        return $houseInfo;
    }
}