<?php

namespace App\Http\Controllers;

use App\Journal;
use App\Submission;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Service\ArticleService;

class ArticleController extends Controller
{
    const JOURNAL_NOT_FOUND = 'Journal not found';
    const SUBMISSION_NOT_FOUND = 'Submission not found';

    /**
     * @var ArticleService $article
     */
    protected $article;

    public function __construct(ArticleService $article)
    {
        $this->article = $article;
    }

    /**
     * Gets a list of all the articles for a journal
     * 
     * @param Request $request The Laravel request
     * @param integer $code The journal id
     * 
     * @return A JSON response containing the articles list
     */
    public function getArticles(Request $request, $code)
    {
        $journal = Journal::find($code);
        
        if (null == $journal) {
            return response()->json(array('message' => self::JOURNAL_NOT_FOUND), Response::HTTP_NOT_FOUND);
        }

        try {
            $articles = $this->article->fetchAll($journal);

            return response()->json($articles, Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json(array('message' => $e->getMessage()), Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Gets the metadata of an article
     * 
     * @param Request $request The Laravel request
     * @param integer $code The journal id
     * @param integer $identifier The article id
     * 
     * @return A JSON response containing the article metadata
     */
    public function getArticleMetadata(Request $request, $code, $identifier)
    {
        //TODO Investigate how to properly map a Journal with a Submission
        $journal = Journal::find($code);
        $submission = Submission::find($identifier);

        if (null == $journal) {
            return response()->json(array('message' => self::JOURNAL_NOT_FOUND), Response::HTTP_NOT_FOUND);
        }

        if (null == $submission) {
            return response()->json(array('message' => self::SUBMISSION_NOT_FOUND), Response::HTTP_NOT_FOUND);
        }

        try {
            $metadata = $this->article->getMetadata($submission);

            return response()->json($metadata, Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json(array('message' => $e->getMessage()), Response::HTTP_BAD_REQUEST);
        }
    }

    public function getArticleContent(Request $request, $code, $identifier)
    {
        $journal = Journal::find($code);
        $submission = Submission::find($identifier);

        if (null == $journal) {
            return response()->json(array('message' => self::JOURNAL_NOT_FOUND), Response::HTTP_NOT_FOUND);
        }

        if (null == $submission) {
            return response()->json(array('message' => self::SUBMISSION_NOT_FOUND), Response::HTTP_NOT_FOUND);
        }

        try {
            $content = $this->article->getContent($submission);

            return response()->json($content, Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json(array('message' => $e->getMessage()), Response::HTTP_BAD_REQUEST);
        }
    }

}
