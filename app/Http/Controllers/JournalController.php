<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Exceptions\JournalNotFoundException;
use App\Service\JournalService;
use App\Service\ContactService;
use App\Journal;

class JournalController extends Controller
{
    const JOURNAL_NOT_FOUND = 'Journal not found';

    /**
     * @var JournalService $journal
     */
    protected $journal;

    /**
     * @var ContactService $contact
     */
    protected $contact;

    public function __construct(JournalService $journal, ContactService $contact)
    {
        $this->journal = $journal;
        $this->contact = $contact;
    }

    /**
     * Get the details of a specific journal
     * 
     * @param Request $request The request being processed
     * @param integer $code The ID of the journal to be fetched
     * 
     * @return array The information about the journal in JSON format, according to the API specification
     */ 
    public function getDetails(Request $request, $code)
    {
        $journal = Journal::find($code);
        
        if (null == $journal) {
            return response()->json(array('message' => self::JOURNAL_NOT_FOUND), Response::HTTP_NOT_FOUND);
        }

        try {
            $details = $this->journal->getDetails($journal);

            return response()->json($details, Response::HTTP_OK);
            
        } catch (\Exception $e) {
            return response()->json(array('message' => $e->getMessage()), Response::HTTP_BAD_REQUEST);
        }
    }

    public function getNavigationDetails(Request $request, $code)
    {
        var_dump($request->json());
        die;
    }

    public function getBannerDetails(Request $request, $code)
    {
        var_dump($request->json());
        die;
    }

    public function getCarouselDetails(Request $request, $code)
    {
        var_dump($request->json());
        die;
    }

    public function getSocialDetails(Request $request, $code)
    {
        var_dump($request->json());
        die;
    }

    public function getContactDetails(Request $request, $code)
    {
        $journal = Journal::find($code);
        
        if (null == $journal) {
            return response()->json(array('message' => self::JOURNAL_NOT_FOUND), Response::HTTP_NOT_FOUND);
        }

        try {
            $details = $this->contact->getDetails($journal);

            return response()->json($details, Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json(array('message' => $e->getMessage()), Response::HTTP_BAD_REQUEST);
        }
    }
}
