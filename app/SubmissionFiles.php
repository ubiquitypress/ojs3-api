<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubmissionFiles extends Model
{
    use \LaravelTreats\Model\Traits\HasCompositePrimaryKey;

    protected $primaryKey = ['file_id', 'revision'];

    /**
     * Get the submission for the file
     */
    public function submission()
    {
        return $this->belongsTo('App\Submission', 'submission_id', 'submission_id');
    }

    /**
     * Get the content of a file
     */
    public function content()
    {
        return $this->hasOne('App\SubmissionContents', 'file_id', 'file_id');
    }
}
