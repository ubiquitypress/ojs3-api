<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $primaryKey = 'section_id';

    /**
     * Get the submissions for the section
     */
    public function submissions()
    {
        return $this->hasMany('App\Submission');
    }

      /**
     * Get the settings for the section
     */
    public function settings()
    {
        return $this->hasMany('App\SectionSettings', 'section_id');
    }

}
