<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuthorSettings extends Model
{
    use \LaravelTreats\Model\Traits\HasCompositePrimaryKey;

    protected $primaryKey = ['author_id', 'locale', 'setting_name'];

    /**
     * Get the section for the setting
     */
    public function author()
    {
        return $this->belongsTo('App\Author', 'author_id', 'author_id');
    }
}
