## About OJS3 API

OJS3 API is an application proposed to be built as a plugin of OJS. The idea is having an up to date way of exposing an API for fetching OJS data to 3rd party clients or simple as a way to have important data exposed for sharing in via a REST JSON API.

The API is written using [Laravel framework](https://laravel.com/docs/5.8).

## Structure of the project

The project consists in 2 main entry points:

1. JournalController: For everything related to journal handling like Carousel, Contacts, Social, Navigation, Banner and typical Journal data.

2. ArticleController: For everything related to Articles/Submissions and its content

Alongside those controllers, several files acts as Models and helpers, to know:

## Installation

Clone the repo to the corresponding document root of the web server/projects and run `composer install`, and configure the correct database parameters in `.env` file.

## Models & Services

Inside the `app` folder we'll find several Laravel models that act as entities and helper service classes for holding and processing the required information.

> By convention, Laravel gets data from a table with the same name of the class, and automatically maps the columns in the table 
> with attributes in the model class. So for example, for getting the id of the journal in `Journal`, the sentence would 
> be `$journal->journal_id;`. For more information about it, check Laravel docs.

1. Journal
2. JournalSetting

3. Section
4. SectionSettings

5. Submission
6. SubmissionSettings
7. Author

8. User Standard out of the box Laravel model

The list is separated by new lines to show the relation between them. When we open some model, we may find relations that help the DB to fetch related data from auxiliary tables. 

For example, we can see in `Journal` model that it is related to many `JournalSettings` by a `$this->hasMany('App\JournalSettings', 'journal_id');`. 

More examples of this type of relations in the mentioned [Laravel docs](https://laravel.com/docs/5.8/eloquent).

The services would be:

1. ArticleService
2. ContactService
3. JournalService

Each services provide a layer of processing and handling the required information, in order to keep the controllers as small and with as less responsibilities as possible.

## Routes

The endpoints routes are the following:

* `/journal/{code}` points to `JournalController@getDetails`, with name `journal-details`
* `/journal/{code}/navigation` points to `JournalController@getNavigationDetails`, with name `nav-details`
* `/journal/{code}/contact` points to `JournalController@getContactDetails`, with name `contact-details`
* `/journal/{code:[A-Za-z]+}/banner` points to `JournalController@getBannerDetails`, with name `banner-details`
* `/journal/{code:[A-Za-z]+}/carousel` points to `JournalController@getCarouselDetails`, with name `carousel-details`
* `/journal/{code:[A-Za-z]+}/social` points to `JournalController@getSocialDetails`, with name `social-details`
* `/journal/{code}/article` points to `ArticleController@getArticles`, with name `get-articles`
* `/journal/{code}/article/{identifier}/metadata` points to `ArticleController@getArticleMetadata`, with name `get-article-metadata`
* `/journal/{code}/article/{identifier}/content` points to ,`ArticleController@getArticleContent`, with name `get-article-content`

> Note: At the moment of writing this documentation, not all of the routes were implemented. Check the commits and updates in the repository for more information

## Tests

Integration tests are provided for the controllers. They basically test that the responses of the calls are compatible with the API specification documentation.

They perform an HTTP request and check that the response structure is compatible with the one specified in each endpoint of the [
API specification](https://francescodevirgilio.docs.apiary.io/#reference) 

> Note: Permissions may be needed to access the API spec
