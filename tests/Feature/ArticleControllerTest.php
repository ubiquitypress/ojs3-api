<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ArticleControllerTest extends TestCase
{
    /**
     * GET articles list test
     * 
     * @dataProvider getArticlesProvider
     */
    public function testGetArticles($code, $expectedHttpStatus, $expectedStructure)
    {
        $response = $this->json('GET', route('get-articles', ['code' => $code]));

        $response->assertStatus($expectedHttpStatus);
        $response->assertJsonStructure($expectedStructure);
    }

    /**
     * GET articles metadata
     * 
     * @dataProvider getArticlesMetadataProvider
     */
    public function testGetArticleMetadata($code, $identifier, $expectedHttpStatus, $expectedStructure)
    {
        $response = $this->json('GET', route('get-article-metadata', ['code' => $code, 'identifier' => $identifier]));

        $response->assertStatus($expectedHttpStatus);
        $response->assertJsonStructure($expectedStructure);
    }

    /**
     * GET articles metadata
     * 
     * @dataProvider getArticlesContentProvider
     */
    public function testGetArticleContent($code, $identifier, $expectedHttpStatus, $expectedStructure)
    {
        $response = $this->json('GET', route('get-article-content', ['code' => $code, 'identifier' => $identifier]));

        $response->assertStatus($expectedHttpStatus);
        $response->assertJsonStructure($expectedStructure);
    }

    public function getArticlesProvider()
    {
        $struct = [
            '*' => [
                'metadata' => [
                    'id',
                    'doi',
                    'section',
                    'date_submitted',
                    'date_accepted',
                    'date_published',
                    'peer_reviewed',
                    'competing_interests',
                    'authors' => [
                        'first_name',
                        'middle_name',
                        'last_name',
                        'email',
                    ],
                ],
                'content' => [
                    '*' => [
                        'file_id',
                        'revision',
                        'content'
                    ]
                ]
            ]
        ];

        $emptyStruct = ['message'];

        return array(
            array(1, Response::HTTP_OK, $struct),
            array('sta', Response::HTTP_NOT_FOUND, $emptyStruct),
        );
    }

    public function getArticlesMetadataProvider()
    {
        $struct = [
            'id',
            'title',
            'doi',
            'submitter' => [
                'first_name',
                'last_name',
                'email',
            ],
            'section',
            'date_submitted',
            'date_accepted',
            'date_published',
            'peer_reviewed',
            'competing_interests',
            'authors' => [
                'first_name',
                'middle_name',
                'last_name',
                'email',
            ],
        ];

        $emptyStruct = ['message'];

        return array(
            array(1, 2, Response::HTTP_OK, $struct),
            array('sta', 2, Response::HTTP_NOT_FOUND, $emptyStruct),
        );
    }

    public function getArticlesContentProvider()
    {
        $struct = [
            '*' => [
                'file_id',
                'revision',
                'content'
            ]
        ];

        $emptyStruct = ['message'];

        return array(
            array(1, 4, Response::HTTP_OK, $struct),
            array('sta', 'sta', Response::HTTP_NOT_FOUND, $emptyStruct),
        );
    }
}
