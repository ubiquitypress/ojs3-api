<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

/**
 * JournalController integration test. 
 * Class to test the API endpoints, as a whole request proccess.
 * The test is intended to check that the response is compatible to the one requested by the API.
 */
class JournalControllerTest extends TestCase
{
    const JOURNAL_NOT_FOUND = 'Journal not found';

    /**
     * GET journal details test
     * 
     * @dataProvider getDetailsProvider
     */
    public function testGetDetails($code, $expectedHttpStatus, $expectedStructure)
    {
        $response = $this->json('GET', route('journal-details', ['code' => $code]));

        $data = $response->decodeResponseJson();

        $response->assertStatus($expectedHttpStatus);
        $response->assertJsonStructure($expectedStructure);
    }

    /**
     * GET contact details test
     * 
     * @dataProvider getContactDetailsProvider
     */
    public function testGetContactDetails($code, $expectedHttpStatus, $expectedStructure)
    {
        $response = $this->json('GET', route('contact-details', ['code' => $code]));

        $data = $response->decodeResponseJson();

        $response->assertStatus($expectedHttpStatus);
        $response->assertJsonStructure($expectedStructure);
    }

    public function getDetailsProvider()
    {
        $struct = [
            'title',
            'initials',
            'description',
            'sections',
            'privacy_policy',
            'eissn',
            'print_issn',
            'published_by',
            'default_language',
        ];

        $emptyStruct = ['message'];

        return array(
            array(1, Response::HTTP_OK, $struct),
            array('sta', Response::HTTP_NOT_FOUND, $emptyStruct),
        );
    }

    public function getContactDetailsProvider()
    {
        $struct = [
            'mailing' => [
                'text' => [
                    'street',
                    'house_number',
                    'postal_code',
                    'city',
                    'text',
                ],
            ],
            'principal',
            'support',
        ];

        $emptyStruct = ['message'];

        return array(
            array(1, Response::HTTP_OK, $struct),
            // array('sta', Response::HTTP_NOT_FOUND, $emptyStruct),
        );
    }
}
